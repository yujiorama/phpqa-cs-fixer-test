#  FriendsOfPHP/PHP-CS-Fixer のテスト

[FriendsOfPHP/PHP-CS-Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer)

指定したアプリケーションに `FriendsOfPHP/PHP-CS-Fixer` で書式チェックを実行します。

```bash
$ ./lint.sh /path/too/app/root | tee result.xml
```

出力形式やルールをカスタマイズするときは `.php_cs` ファイルを編集します。
