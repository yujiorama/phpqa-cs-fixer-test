FROM phpqa/php-cs-fixer

ARG APP=cakephp-realworld-example-app
WORKDIR /app
COPY ${APP}/ ./
COPY .php_cs ./
