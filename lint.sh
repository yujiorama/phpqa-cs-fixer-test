#!/bin/bash

if [[ $# -lt 1 ]]; then
    echo "${BASH_SOURCE[0]} <app>(host directory)"
    exit 1
fi
app="$1"
if [[ ! -d "${app}" ]]; then
    echo "${BASH_SOURCE[0]} <app>(host directory)"
    exit 1
fi

set -euo pipefail
docker build -t php-cs-fixer:debug --build-arg APP="${app}" . >/dev/nul 2>&1
docker run --rm -it php-cs-fixer:debug fix . --config .php_cs -v --dry-run --diff | tail -n +2
docker rmi php-cs-fixer:debug >/dev/nul 2>&1
